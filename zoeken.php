<?php
	// Dit commando zorgt voor de verbinding met de database.
	require('database.inc');
	
	// De titel van de pagina, die bovenaan en in de menu-balk verschijnt.
	$title = 'Vluchtinformatie opzoeken';
	
	// Dit commando zorgt voor de initialisatie van de pagina en
	// het weergeven van het menu.
	require("top.inc");
?>

<!-- Dit is het formulier om te zoeken op aantal stops: -->
<form action="zoekopaantalstops.php">
<em>aantal stops:</em> <input type="text" name="aantalstops" length="2"/><br />

<!-- De knop waarop de gebruiker kan klikken. -->
<input type="submit" value="Zoek op aantal stops"/>
</form>
<br />
<!-- Dit is het formulier om te zoeken op vertrektijd: -->
<form action="zoekopvertrektijd.php">
<label for="vertrektijd">Geef de dag van vertrek in aan de hand van het volgende formaat: JJJJ-MM-DD</label>
<br />
<em>vertrektijd:</em> <input type="text" name="vertrektijd" length="10"/><br />

<!-- De knop waarop de gebruiker kan klikken. -->
<input type="submit" value="Zoek op vertrektijd"/>
</form>
<br />
<!-- Dit is het formulier om te zoeken op oorsprong: -->
<form action="zoekopoorsprong.php">
<em>oorsprong:</em> <input type="text" name="oorsprong"/><br />

<!-- De knop waarop de gebruiker kan klikken. -->
<input type="submit" value="Zoek op oorsprong"/>
</form>
<br />
<!-- Dit is het formulier om te zoeken op oorsprong: -->
<form action="zoekopoorsprongPART1.php">
<em>oorsprong:</em> <input type="text" name="oorsprong"/><br />

<!-- De knop waarop de gebruiker kan klikken. -->
<input type="submit" value="Zoek op oorsprong (met vluchttijd)"/>
</form>

<?php
// Dit sluit de verbinding met de gegevensbank en de pagina af.
require("bottom.inc");
?>
<?php
	// Dit commando zorgt voor de verbinding met de database.
	require('database.inc');

	// De titel van de pagina, die bovenaan en in de menu-balk verschijnt.
	$title = 'Een vlucht boeken';

	// Dit commando zorgt voor de initialisatie van de pagina en
	// het weergeven van het menu.
	require("top.inc");
?>

<!-- Dit is het formulier om de ontlening te bevestigen: -->
<form action="boeking_uitvoer.php" method="post">

<!-- Voeg hier je code toe -->
<table>
<thead>
<tr><th>Vluchtnr.</th><th>Van</th><th>Vertrektijd</th><th>Naar</th><th>Aankomsttijd</th><th>Zitplaats</th></tr>
</thead>
<tbody>
<?php
	//opvangen en opslaan van de klant (als die parameter gedefinieerd is)
	if(isset($_POST["klant"])) { $_SESSION['reiziger'] = $_POST["klant"]; };

	//functie om voor een bepaalde vlucht, $vVlucht, de vrije zitjes (nummer en klasse) terug te geven
	function query_zitjes($vVlucht) {
		return "SELECT z.Zitplaats_Nr, z.Luchtvaartmaatschappij_ID, z.Klasse FROM zitplaats as z LEFT JOIN wordtgeboektdoor as b ON (z.Vlucht_Nr = b.Vlucht_Nr AND z.Zitplaats_Nr = b.Zitplaats_Nr) WHERE (z.Vlucht_Nr = " . $vVlucht . " AND b.Zitplaats_Nr IS NULL)";
	}

	//functie die vluchten opvraagt (met vertrek- een bestemmingsluchthaven) waarvoor nog zitjes vrij zijn (met een limiet en een offset)
	function query_vluchten($result_offset,$results_per_page) {
		return "SELECT v.Vlucht_Nr, v.Vertrektijd, v.Aankomsttijd, v.ToegewezenVliegtuig, v.LuchthavenVanHerkomst, v.LuchthavenVanBestemming, L1.Luchthaven_ID, L1.Naam as L1_Naam, L1.Land as L1_Land, L2.Luchthaven_ID, L2.Naam as L2_Naam, L2.Land as L2_Land FROM vlucht as v, luchthaven as L1, luchthaven as L2 WHERE v.LuchthavenVanHerkomst = L1.Luchthaven_ID AND v.LuchthavenVanBestemming = L2.Luchthaven_ID AND EXISTS (" . query_zitjes("v.Vlucht_Nr") . ") ORDER BY v.Vertrektijd ASC LIMIT $result_offset,$results_per_page";
	}

	//functie om de resultaten per 10 weer te geven op een pagina
	function pager() {
		global $page;
		global $page_amount;
		global $vluchten_resultaat;

		//de parameter p geeft het paginanummer aan (als p niet meegegeven werd is p 1)
		if(!isset($_GET["p"]) || $_GET["p"] === "" || ($_GET["p"] < 1) ) {
			$page = 1;
		} else {
			$page = $_GET["p"];
		}

		//alle resultaten opvragen en tellen om het aantal pagina's te kennen (dus met een range van 0 tot de maximale integer-waarde in PHP)
		$results_per_page = PHP_INT_MAX;
		$result_offset = 0;
		$vluchten_resultaat = mysql_query(query_vluchten($result_offset,$results_per_page)) or die("Kan de lijst van vluchten niet opvragen: " . mysql_error());
		$num_results = mysql_num_rows($vluchten_resultaat);

		//bepalen van de range van resultaten voor de huidige pagina en het opvragen van die resultaten
		$results_per_page = 10;
		$page_amount = ceil($num_results/$results_per_page);
		$result_offset = ($page - 1)*$results_per_page;
		$vluchten_resultaat = mysql_query(query_vluchten($result_offset,$results_per_page)) or die("Kan de lijst van vluchten niet opvragen: " . mysql_error());
	}

	//aanroepen van de hiervoor uitgewerkte functie
	pager();

	//itereren over alle vluchten met lege plaatsen
	while($rij = mysql_fetch_array($vluchten_resultaat)) {
		$vVlucht_Nr = $rij['Vlucht_Nr'];
		//opvragen van de lege zitjes op de vlucht
		$zitjes_resultaat = mysql_query(query_zitjes($vVlucht_Nr)) or die("Kan de lijst van zitjes niet opvragen: " . mysql_error());
		$zitjes_options = "";
		while($zitje = mysql_fetch_array($zitjes_resultaat)) {
			//itereren over de vrije zitjes als opties bij een vlucht
			$zitjes_options .= "<option value=\"". $zitje[0] . "\">Plaats " .$zitje['Zitplaats_Nr'] . ' ' . $zitje['Klasse'] . ', ' . $zitje['Luchtvaartmaatschappij_ID'] . "</option>";
		}
		//iedere vlucht is een rij met informatie en een dropdown met beschikbare plaatsen (uit de vorige lus) die geselecteerd kan worden met een radio-button aan het begin van de regel
		//de waarde die voor de vlucht gepost wordt is het vluchtnummer, de gekozen plaats wordt gepost met een naam die afgeleid is van het vluchtnummer (door het suffix 'zitje')
		echo "<tr><td><input type=\"radio\" name=\"vlucht\" value=\"". $rij['Vlucht_Nr'] . "\">" .$rij['Vlucht_Nr'] . "</td><td>" . $rij['L1_Naam'] . ", " . $rij['L1_Land'] . "</td><td>" . explode('.', $rij['Vertrektijd'])[0] . "</td><td>" . $rij['L2_Naam'] . ", " . $rij['L2_Land'] . "</td><td>" . explode('.', $rij['Aankomsttijd'])[0] . "</td><td><select name=\"" .$rij['Vlucht_Nr'] . "zitje\">" . $zitjes_options . "</select></td></tr>";
	}
?>
</tbody>
</table>
<style>
table
{
border-collapse:collapse;
}
table, th, td
{
border: 1px solid black;
padding: 8px;
}
</style>
<input type="submit" value="Ga verder"/>
</form>
<?php
if($page_amount != "0"){
		echo "<div class=paging>";
			if($page != "1"){
				$prev = $page-1;
				echo "<a href=\"boeking_selecteervlucht.php?p=$prev\">Vorige</a>";
			}
			if($page < $page_amount){
				$next = $page+1;
				echo "<a href=\"boeking_selecteervlucht.php?p=$next\">Volgende</a>";
			}
		echo "</div>";
	}
?>

<?php
// Dit sluit de verbinding met de gegevensbank en de pagina af.
require("bottom.inc");
?>
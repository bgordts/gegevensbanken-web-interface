<?php
  // Dit commando zorgt voor de verbinding met de database.
  require('database.inc');

  $query1 = "DROP VIEW IF EXISTS promoties;";
  $result1 = mysql_query($query1) or die("Database fout: " . mysql_error());

  $query2 = "CREATE VIEW promoties AS
        SELECT v.Vlucht_Nr, v.Vertrektijd, v.Aankomsttijd, v.ToegewezenVliegtuig,
           v.LuchthavenVanHerkomst, v.LuchthavenVanBestemming, L1.Luchthaven_ID as Lid1,
           L1.Naam as L1_Naam, L1.Land as L1_Land, L2.Luchthaven_ID as Lid2, L2.Naam as L2_Naam,
           L2.Land as L2_Land
        FROM vlucht as v, luchthaven as L1, luchthaven as L2
        WHERE   v.LuchthavenVanHerkomst = L1.Luchthaven_ID AND v.LuchthavenVanBestemming = L2.Luchthaven_ID
          AND EXISTS    ( SELECT z.Zitplaats_Nr, z.Luchtvaartmaatschappij_ID, z.Klasse
             FROM zitplaats as z LEFT JOIN wordtgeboektdoor as b ON (z.Vlucht_Nr = b.Vlucht_Nr AND z.Zitplaats_Nr = b.Zitplaats_Nr)
             WHERE (z.Vlucht_Nr = v.vlucht_Nr AND b.Zitplaats_Nr IS NULL) AND
                        ( 0 < DATEDIFF(v.Vertrektijd, NOW()) AND DATEDIFF(v.Vertrektijd, NOW()) <= 21) );" ;

  $result2 = mysql_query($query2) or die("Database fout: " . mysql_error());

?>

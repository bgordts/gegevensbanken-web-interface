<?php
  // Dit commando zorgt voor de verbinding met de database.
  require('database.inc');

  // De titel van de pagina, die bovenaan en in de menu-balk verschijnt.
  $title = 'Zoek promoties';

  // Dit commando zorgt voor de initialisatie van de pagina en
  // het weergeven van het menu.
  require("top.inc");

  $startB = isset($_POST["startB"]) ? $_POST["startB"] : "";
  $eindB = isset($_POST["eindB"]) ? $_POST["eindB"] : "";
  $vertrekU = isset($_POST["vertrekU"]) ? $_POST["vertrekU"] : "";
  $aankomstU = isset($_POST["aankomstU"]) ? $_POST["aankomstU"] : "";

  $ordening = isset($_POST["ordening"]) ? $_POST["ordening"] : "";
  $descending = isset($_POST['descending']) ? $_POST['descending'] : "";


  $query = "  SELECT *
        FROM promoties as p
        WHERE TRUE AND ";
  if($eindB != "")
    $query = $query."p.L2_Naam= '".$eindB."' AND ";
  if($startB != "")
    $query = $query."p.L1_Naam= '".$startB."' AND ";
  if($vertrekU != "")
    $query = $query."DATE_FORMAT(p.Vertrektijd,'%Y-%m-%d')= '".$vertrekU."' AND ";
  if($aankomstU != "")
    $query = $query."DATE_FORMAT(p.Aankomsttijd,'%Y-%m-%d')= '".$aankomstU."' AND ";
  $query = substr($query, 0,-5);
  if($ordening != "")
    $query = $query." ORDER BY ".$ordening." ".$descending;

  $vluchten_resultaat = mysql_query($query) or die("Database fout: " . mysql_error());

?>

<form action="promotieFilter.php" method="post">
<em>Vertrekluchthaven</em> <input type="text" name="startB"/><br />
<em>Bestemming</em> <input type="text" name="eindB"/><br />
<em>Vertrekuur</em> <input type="text" name="vertrekU"/><br />
<em>Aankomstuur</em> <input type="text" name="aankomstU"/><br />

<em>Sorteren op:</em>
<select name="ordening">
  <option value="L1_Naam">Vertrekluchthaven</option>
  <option value="L2_Naam">Bestemming</option>
  <option value="Vertrektijd">Vertrekuur</option>
  <option value="Aankomsttijd">Aankomstuur</option>
</select>
<select name="descending">
  <option value="ASC">Oplopend</option>
  <option value="DESC">Aflopend</option>
</select>

<!-- De knop waarop de gebruiker kan klikken. -->
<input type="submit" value="Promoties zoeken"/>
</form>

<!-- Voeg hier je code toe -->
<table>
<thead>
<tr><th>Vluchtnr.</th><th>Van</th><th>Vertrektijd</th><th>Naar</th><th>Aankomsttijd</th><th>Zitplaats</th><th>Korting</th></tr>
</thead>
<tbody>

<?php

  //functie om voor een bepaalde vlucht, $vVlucht, de vrije zitjes (nummer en klasse) terug te geven
  function query_zitjes($vVlucht) {
    return "SELECT z.Zitplaats_Nr, z.Luchtvaartmaatschappij_ID, z.Klasse FROM zitplaats as z LEFT JOIN wordtgeboektdoor as b ON (z.Vlucht_Nr = b.Vlucht_Nr AND z.Zitplaats_Nr = b.Zitplaats_Nr) WHERE (z.Vlucht_Nr = " . $vVlucht . " AND b.Zitplaats_Nr IS NULL)";
  }

  //itereren over alle vluchten met lege plaatsen
  while($rij = mysql_fetch_array($vluchten_resultaat)) {
    $vVlucht_Nr = $rij['Vlucht_Nr'];
    //opvragen van de lege zitjes op de vlucht
    $zitjes_resultaat = mysql_query(query_zitjes($vVlucht_Nr)) or die("Kan de lijst van zitjes niet opvragen: " . mysql_error());
    $zitjes_options = "";
    while($zitje = mysql_fetch_array($zitjes_resultaat)) {
      //itereren over de vrije zitjes als opties bij een vlucht
      $zitjes_options .= "<option value=\"". $zitje[0] . "\">Plaats " .$zitje['Zitplaats_Nr'] . ' ' . $zitje['Klasse'] . ', ' . $zitje['Luchtvaartmaatschappij_ID'] . "</option>";
    }

    $datediff = date_diff( date_create_from_format('Y-m-d H:i:s', date('Y-m-d H:i:s')), date_create_from_format('Y-m-d H:i:s', $rij['Vertrektijd']) )->format('%d');
    if($datediff < 8) {
      $korting = "20%";
    } elseif($datediff < 15) {
      $korting = "10%";
    } else {
      $korting = "5%";
    };

    //iedere vlucht is een rij met informatie en een dropdown met beschikbare plaatsen (uit de vorige lus) die geselecteerd kan worden met een radio-button aan het begin van de regel
    //de waarde die voor de vlucht gepost wordt is het vluchtnummer, de gekozen plaats wordt gepost met een naam die afgeleid is van het vluchtnummer (door het suffix 'zitje')
    echo "<tr><td>" .$rij['Vlucht_Nr'] . "</td><td>" . $rij['L1_Naam'] . ", " . $rij['L1_Land'] . "</td><td>" . explode('.', $rij['Vertrektijd'])[0] . "</td><td>" . $rij['L2_Naam'] . ", " . $rij['L2_Land'] . "</td><td>" . explode('.', $rij['Aankomsttijd'])[0] . "</td><td><select name=\"" .$rij['Vlucht_Nr'] . "zitje\">" . $zitjes_options . "</select></td><td>".$korting."</td></tr>";
  }
?>

</tbody>
</table>

<style>
table
{
border-collapse:collapse;
}
table, th, td
{
border: 1px solid black;
padding: 8px;
}
</style>

<?php
// Dit sluit de verbinding met de gegevensbank en de pagina af.
require("bottom.inc");
?>



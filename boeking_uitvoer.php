<?php
	// Dit commando zorgt voor de verbinding met de database.
	require('database.inc');

	// De titel van de pagina, die bovenaan en in de menu-balk verschijnt.
	$title = 'Een vlucht boeken';

	// Dit commando zorgt voor de initialisatie van de pagina en
	// het weergeven van het menu.
	require("top.inc");
?>

<!-- Voeg hier je code toe -->
<?php
	//opslaan van de vluchtnummer en het zitje
	$_SESSION['BoekingVluchtNr'] = $_POST["vlucht"];
	$_SESSION['BoekingZitje'] = $_POST[ $_SESSION['BoekingVluchtNr'] . 'zitje' ];

	//opvragen van de luchtvaartmaatschappij-id
	$luchtvaartmaatschappij_ID_query = "SELECT Zitplaats_Nr, Luchtvaartmaatschappij_ID, Vlucht_Nr FROM zitplaats WHERE Zitplaats_Nr = " . $_SESSION['BoekingZitje'] . " AND Vlucht_Nr = " . $_SESSION['BoekingVluchtNr'] . "";
	//echo "query " . $query;

	$resultaat = mysql_query($luchtvaartmaatschappij_ID_query) or die("Kan de lijst van klanten niet opvragen: " . mysql_error());
	while($rij = mysql_fetch_array($resultaat)) {
		$_SESSION['BoekingLuchtvaartmaatschappij'] = $rij['Luchtvaartmaatschappij_ID'];
	}

	$insert_query = "INSERT INTO wordtgeboektdoor VALUES (" . $_SESSION['reiziger'] . "," . $_SESSION['boekingsbureau'] . "," . $_SESSION['BoekingZitje'] . "," . $_SESSION['BoekingLuchtvaartmaatschappij'] . "," . $_SESSION['BoekingVluchtNr'] . ")";

	$result = mysql_query($insert_query) or die("Database fout: " . mysql_error());


	//opzoeken prijs zitplaats
	$price_query = "SELECT k.prijs FROM zitplaats as z, klasse as k WHERE z.Zitplaats_Nr = " . $_SESSION['BoekingZitje'] . " AND z.Vlucht_Nr = " . $_SESSION['BoekingVluchtNr'] . " AND z.Klasse = k.type";
	$price_resultaat = mysql_query($price_query) or die("Kan de lijst van klanten niet opvragen: " . mysql_error());
	while($rij = mysql_fetch_array($price_resultaat)) {
		$prijs = $rij['prijs'];
	}

	//opzoeken tax
	$tax_query = "SELECT L1.Tax as tax1, L2.Tax as tax2 FROM luchthaven as L1, luchthaven as L2, vlucht as v WHERE v.Vlucht_Nr = " . $_SESSION['BoekingVluchtNr'] . " AND L1.Luchthaven_ID = v.LuchthavenVanHerkomst AND L2.Luchthaven_ID = v.LuchthavenVanBestemming";
	$tax_resultaat = mysql_query($tax_query) or die("Kan de lijst van klanten niet opvragen: " . mysql_error());
	while($rij = mysql_fetch_array($tax_resultaat)) {
		$tot_tax = $rij['tax1'] + $rij['tax2'];
		$tot_prijs = $tot_tax;

		echo "De totale prijs bedraagt: " . $tot_prijs;
	}

?>

<?php
// Dit sluit de verbinding met de gegevensbank en de pagina af.
require("bottom.inc");
?>
<?php
//Zien dat we aan de database geraken
require('../database.inc');
//Iemand zit nog op een oudere php
require('password.php');

//De sessie starten waar we de user aan zullen herkennen
session_start();

// Is de werknemer al ingelogd?
if(isset( $_SESSION['user_id'] ))
{
    $message = 'De werknemer is al ingelogd!';
}
//Zijn beide velden ingevuld?
if(empty( $_POST['username']) || empty($_POST['password']))
{
    $message = 'Gelieve een gebruikersnaam en/of passwoord in te vullen';
}
else
{
    //Nooit gebruikers vertrouwen (SQL Injections bijvoorbeeld)
    $username = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
    $password = filter_var($_POST['password'], FILTER_SANITIZE_STRING);

    try
    {
        $query = "SELECT id, username, password FROM Werknemer WHERE username = '".$username."';";
        $result = mysql_query($query);
        $entry = mysql_fetch_array($result, MYSQL_ASSOC);

        //Het passwoord verifieren (we gebruiken BCrypt om passwoorden te encoderen)
        $login_correct = password_verify($password, $entry['password']); 

        //Als het passwoord fout is
        if(!$login_correct)
        {
                $message = 'Login Failed';
        }
        //Anders is alles in orde
        else
        {
                //Voeg het user id toe aan de sessie
                $_SESSION['user_id'] = $entry['id'];

                $message = 'You are now logged in';

                $url = 'http://' . $_SERVER['HTTP_HOST'];            // Get the server
                $url .= rtrim(dirname($_SERVER['PHP_SELF']), '/\\'); // Get the current directory
                $url .= '/../';            // <-- Your relative path
                header('Location: ' . $url, true, 302);   
        }

    }
    catch(Exception $e)
    {
        /*** if we are here, something has gone wrong with the database ***/
        $message = 'We are unable to process your request. Please try again later'.$e->getMessage();
    }
}
?>

<html>
<head>
<title>PHPRO Login</title>
</head>
<body>
<p><?php echo $message; ?>
</body>
</html>
<?php
	function isLoggedIn(){
		return isset($_SESSION['user_id']);
	}

	function redirectToHome(){
		$url = 'http://' . $_SERVER['HTTP_HOST'];            // Get the server
		$url .= rtrim(dirname($_SERVER['PHP_SELF']), '/\\'); // Get the current directory

		//Als de URL al 'users' bevat, dit er uit halen
		if(strpos($url,'users')){
			$url .= '/../';            // <-- Your relative path
		}
		
		header('Location: ' . $url, true, 302);
		die();
	}

	function redirectToLogin(){
		//De gebruiker is niet ingelogd, verwijs hem door naar de log in pagina
		$url = 'http://' . $_SERVER['HTTP_HOST'];            // Get the server
		$url .= rtrim(dirname($_SERVER['PHP_SELF']), '/\\'); // Get the current directory

		//Als de URL al 'users' bevat, niet nog eens toevoegen
		if(!strpos($url,'users')){
			$url .= '/users/login.php';            // <-- Your relative path
		} else{
			$url .= '/login.php';  
		}
		
		header('Location: ' . $url, true, 302);
		die();
	}
?>
<?php
require('user_util.php');

//Als de gebruiker al ingelogd is, verwijs hem dan door naar de homepagina
if(isLoggedIn()){
	redirectToHome();
}

//Als deze pagina geladen wordt, moet deze code niet uigevoerd wordt. Als deze pagina aangeroepen wordt met
//form data, dan moet deze logica wel uitgevoerd worden.
if (!empty($_POST)){
	//Zien dat we aan de database geraken
	require('../database.inc');
	//Iemand zit nog op een oudere php
	require('password.php');

	//De sessie starten waar we de user aan zullen herkennen
	session_start();

	// Is de werknemer al ingelogd?
	if(isset( $_SESSION['user_id'] ))
	{
	    $message = 'Je bent al ingelogd!';
	}
	//Zijn beide velden ingevuld?
	if(empty( $_POST['username']) || empty($_POST['password']))
	{
	    $message = 'Gelieve een gebruikersnaam en een wachtwoord in te vullen.';
	}
	else
	{
	    //Nooit gebruikers vertrouwen (SQL Injections bijvoorbeeld)
	    $username = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
	    $password = filter_var($_POST['password'], FILTER_SANITIZE_STRING);

        $query = "SELECT id, username, password FROM Werknemer WHERE username = '".$username."';";
        $result = mysql_query($query);
        $entry = mysql_fetch_array($result, MYSQL_ASSOC);

        //Checken of de gegeven gebruikersnaam wel in onze database zit
        if(!$entry){
        	$message = "De gebruikersnaam is niet correct.";
        } else{
        	//Het passwoord verifieren (we gebruiken BCrypt om passwoorden te encoderen)
	        $login_correct = password_verify($password, $entry['password']); 

	        //Als het passwoord fout is
	        if(!$login_correct)
	        {
	                $message = 'Het gegeven wachtwoord is onjuist.';
	        }
	        //Anders is alles in orde
	        else
	        {
	                //Voeg het user id toe aan de sessie
	                $_SESSION['user_id'] = $entry['id'];

	                //Redirecten naar de indexpagina
	                redirectToHome();
	        }
        }
	}
}
?>

<html> 
	<head> 
		<title>Inloggen</title> 
	</head> 
	<body> 
		<h2>Inloggen</h2> 
		<div id="statusMessage">	
			<p><?php echo $message; ?></p>
		</div>

		<form action="login.php" method="post"> 
			<fieldset> 
				<p> 
					<label for="username">Gebruikersnaam</label> 
					<input type="text" id="username" name="username" value="" maxlength="20" /> 
				</p> 
				<p> 
					<label for="password">Wachtwoord</label> 
					<input type="password" id="password" name="password" value="" maxlength="20" /> 
				</p>
				<p> 
					<input type="submit" value="Inloggen" /> 
				</p> 
			</fieldset> 
		</form> 
	</body> 
</html>
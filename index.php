<?php
	// Voer de inhoud van "database.inc" uit. Dit is PHP-code
	// die de verbinding met de database aangaat.
	require('database.inc');

	// Dit is de titel die op de pagina en in de menubalk
	// zal verschijnen.
	$title = "Luchtvaart database";

	// Voer de inhoud van "top.inc" uit. Deze verzorgt de
	// algemene pagina lay-out en het menu.
	require("top.inc");
?>

Dit is een web-pagina.<br /><br />

<?php
	//Een random getal tussen 1 en 20 retourneren
	echo "Random getal tussen 1 en 20: ".rand(1,20)."<br /><br />";

	//Vandaag:
	$date = date("Y/m/d");

	//De dag van vandaag printen
	echo "Vandaag: ".$date."<br />";

	//Volgende week
	echo "Volgende week: ".date("Y/m/d",strtotime("+1 week"))."<br />";
	//Vorige week
	echo "Vorige week: ".date("Y/m/d",strtotime("-1 week"))."<br />";
?>.

<?php
// Voer de inhoud van "bottom.inc" uit. Dit sluit de pagina af
// en verbreekt de verbinding met de database.
require("bottom.inc");
?>
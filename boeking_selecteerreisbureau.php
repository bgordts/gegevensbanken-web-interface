<?php
	// Dit commando zorgt voor de verbinding met de database.
	require('database.inc');

	// De titel van de pagina, die bovenaan en in de menu-balk verschijnt.
	$title = 'Een vlucht boeken';

	// Dit commando zorgt voor de initialisatie van de pagina en
	// het weergeven van het menu.
	require("top.inc");
?>

<!-- Dit is het formulier om de klant in te geven: -->
<form action="boeking_selecteerklant.php" method="post">

<!-- Voeg hier je code toe -->
<p><em>Reisbureau:</em>
<select name="reisbureau">
<?php
	$query = "SELECT Reisbureau_ID, Straat, Nummer, Bus, Postcode, Stad FROM reisbureau";
	$resultaat = mysql_query($query) or die("Kan de lijst van reisbureau's niet opvragen: " . mysql_error());
	while($rij = mysql_fetch_array($resultaat)) {
		echo "<option value=\"". $rij['Reisbureau_ID'] . "\">" . $rij['Straat'] . ' ' . $rij['Nummer'] . ' ' . $rij['Bus'] . ', ' . $rij['Postcode'] . ' ' . $rij['Stad'] . "</option>";
	}
?>
</select></p>
<input type="submit" value="Ga verder"/>
</form>

<?php
// Dit sluit de verbinding met de gegevensbank en de pagina af.
require("bottom.inc");
?>